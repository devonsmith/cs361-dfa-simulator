﻿using System;
using System.Collections.Generic;
using System.Linq;

#region "Project Statement"
/**************************************************************************************************
* Project Statement:
* 
* Implement a simulator for a Deterministic Finite Automaton (DFA). This should be a 
* generic simulator that can simulate a variety of Automata based on a constructor.
* 
* Implement the following DFA:
*  
*          _        _  
*         |0|      |1|
* 		  | v      | v
*         ***  1   ***
*   ---> ** **--->*   *
*         ***      ***
* 		   ^        |
* 		   |        |
* 		    --------
*              0
* 
* Note: Accepts even numbers and empty strings.
* 
* Test against the following values:
*     10101
*     0010
*     0010100
*     1000
*     ε (the empty string)
* 
* Personal Addendum: On the quiz there was a question about creating an automata that 
* accepted any number with three consecutive ones.
* 
* This is the reg ex: (0|1)*(111)+(0|1)*
* 
* For fun I implemented this DFA as well using the same Automata class to make sure 
* it could create valid DFAs with the appropriate arguments.
* 
***************************************************************************************************/

namespace CS361___DFA_Simulator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Output the application header
            Console.WriteLine("=================================================================" +
                              "=======");
            Console.WriteLine("Deterministic Finite Automata Simulation");
            Console.WriteLine("=================================================================" +
                              "=======\n");

            // In this demo application the alphabet will be the same for both machines.
            char[] alphabet = { '0', '1' };

            /***************************************************************************
            * Deterministic Finite Automata One
            ***************************************************************************/

            // Simulate even numbers and empty strings.
            Console.WriteLine("-----------------------------------------------------------------" +
                              "-------");
            Console.WriteLine("Simulation 1: Simulate a Deterministic Finite Automata that " +
                              "accepts even \nnumbers and empty strings.");
            Console.WriteLine("-----------------------------------------------------------------" +
                              "-------\n");
            
            // Create an array of states
            int[] m1States = { 0, 1 };
            // Create an array for the state adjacency
            int[,] m1StateTable = { { 0, 1 }, { 0, 1 } };

            // start state is 0
            // final state is 0
            int[] m1Finals = { 0 };

            Tuple<int[], char[], int[,], int, int[]> input =
                new Tuple<int[], char[], int[,], int, int[]>
                (m1States, alphabet, m1StateTable, 0, m1Finals);

            // Test Values as provided by the problem statement.
            string[] m1TestValues = { "10101", "0010", "0010100", "1000", "" };

            // Create an Automata
            Automata machine = new Automata(input);
            foreach (string s in m1TestValues)
            {
                Console.WriteLine("The string provided {0}, {1} the language.",
                    (String.IsNullOrEmpty(s) ? "" + '\u03B5' : s),
                    (machine.processString(s) ? "matches" : "does not match"));
            }
            // add some space to the console.
            Console.WriteLine("\n");

            /***************************************************************************
            * Deterministic Finite Automata One
            ***************************************************************************/

            /* Note: This second simulation was just for fun but was on the quiz so I thought it would 
             * be a good way to test the constructors for my automata simulation  */

            Console.WriteLine("-----------------------------------------------------------------" +
                              "-------");
            Console.WriteLine("Simulation 2: Simulate a Deterministic Finite Automata that " +
                              "accepts any \nstring with three consecutive 1s");
            Console.WriteLine("-----------------------------------------------------------------" +
                              "-------\n");

            // array of states for the machine
            int[] m2States = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
            // Array of states adjacency.
            int[,] m2StateTable = { { 1, 2 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { 5, 6 },
                                    { 5, 7 }, { 4, 6 }, { 5, 8 }, { 5, 6 } };
            // States 4, 5, 7, 7, and 8 are all final states.
            int[] m2Finals = { 4, 5, 6, 7, 8 };
            // Test values to run against the machine.
            string[] m2TestValues = { "10101", "10111", "101101110101", "01110", "11010110001", "" };

            Tuple<int[], char[], int[,], int, int[]> m2Input =
                new Tuple<int[], char[], int[,], int, int[]>
                (m2States, alphabet, m2StateTable, 0, m2Finals);

            Automata machine2 = new Automata(m2Input);
            foreach (string s in m2TestValues)
            {
                Console.WriteLine("The string provided {0}, {1} the language.",
                    (String.IsNullOrEmpty(s) ? "" + '\u03B5' : s),
                    (machine2.processString(s) ? "matches" : "does not match"));
            }

            Console.WriteLine("\n\nDFA Simulations Complete!\n\n");
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
    #region "classes"
    #region "Automata State"
    /// <summary>
    /// This is a generic class for creating a state for an Automata.
    /// </summary>
    class AutomataState
    {
        private Dictionary<char, AutomataState> stateList;
        // Is this a possible final state?
        private bool finalState;


        // public property for final state.
        public bool isFinal
        {
            // return the final state boolean value.
            get { return finalState; }
            // set the boolean state final value.
            set { finalState = value; }
        }

        // Constructor
        public AutomataState()
        {
            // Initialize the state dictionary
            stateList = new Dictionary<char, AutomataState>();
            // By default, this is not going to be a final state. 
            finalState = false;

        }

        // Constructor allowing the creation with the setting is the state is final.
        public AutomataState(bool final)
        {
            stateList = new Dictionary<char, AutomataState>();
            finalState = final;
        }

        // Adds a state to the state list that will process the next character in the chain.
        public void addState(char character, AutomataState state)
        {
            stateList[character] = state;
        }

        public bool processString(string str)
        {
            char current;
            if (String.IsNullOrEmpty(str))
            {
                return (finalState) ? true : false;
            }
            else
            {
                current = str.First();
                return stateList[current].processString(str.Substring(1));
            }

        }
    }
    #endregion
    #region "Automata"

    /// <summary>
    /// Class for creating a finite state machine. This will create a machine comprised of states
    /// that will process a string in a way defined during construction.
    /// </summary>
    class Automata
    {
        // array of the states
        private int[] states;
        // array of the alphabet
        private char[] alphabet;
        // array of the possible states
        private int[,] stateArray;
        // start element numerical index.
        private int start;
        // list of the states that are final.
        private int[] finals;

        // This is the table for storing the states for the Automata.
        private AutomataState[] stateTable;

        /// <summary>
        /// constructor accepts a 5-tuple {Q, Σ, δ, q1, F}
        /// </summary>
        /// <param name="m">5-tuple {Q, Σ, δ, q1, F}</param>
        public Automata(Tuple<int[], char[], int[,], int, int[]> m)
        {
             /********************************************************************** 
             * Note: The first, Q, is the numerical representations of the states. 
             * the second, Σ, is the array of the alphabet.
             * the third, δ, is the state table.
             * the fourth, q1, is the start state.
             * the fifth F is an array of the final states. 
             ***********************************************************************/
            
            // unbox the tuple.
            states = m.Item1;
            alphabet = m.Item2;
            stateArray = m.Item3;
            start = m.Item4;
            finals = m.Item5;

            stateTable = new AutomataState[states.Length];
            // We don't actually care what is in the array for the automata, we just care how many
            // there are.
            for (int i = 0; i < states.Length; ++i)
            {
                // create a state for each of the elements in the states array.
                stateTable[i] = new AutomataState();
            }

            // set which states are final states.
            // for each number in the list of final states
            for (int fs = 0; fs < finals.Length; ++fs)
            {
                int index = finals[fs];
                // set the value to final for that state.
                stateTable[index].isFinal = true;
            }

            // create the relationships for the states.
            for (int i = 0; i < stateArray.GetLength(0); ++i)
            {
                // for the items in the array[i,j]
                for (int j = 0; j < stateArray.GetLength(1); ++j)
                {
                    // the index is the element from the state array that corresponds
                    // with a state in the machine.
                    int index = stateArray[i, j];
                    // Add a action to the state for the character in the alphabet.
                    stateTable[i].addState(alphabet[j], stateTable[index]);
                }
            }

        }
        // process the string starting at the first state in the automata.
        public bool processString(string str)
        {
            // return the result of the string processed by the states in the machine.
            return stateTable[start].processString(str);
        }
    }
    #endregion
    #endregion
}
#endregion